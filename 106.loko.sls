;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; SRFI-106 (Sockets)

(library (srfi :106)
  (export make-client-socket make-server-socket
          socket?
          socket-input-port socket-output-port
          call-with-socket
          socket-merge-flags socket-purge-flags
          socket-accept socket-send socket-recv socket-shutdown socket-close
          *af-unspec* *af-inet* *af-inet6*
          *sock-stream* *sock-dgram*
          *ai-canonname* *ai-numerichost*
          *ai-v4mapped* *ai-all* *ai-addrconfig*
          *ipproto-ip* *ipproto-tcp* *ipproto-udp*
          *shut-rd* *shut-wr* *shut-rdwr*
          address-family socket-domain address-info
          ip-protocol message-type shutdown-method)
  (import
    (srfi :106 socket)))

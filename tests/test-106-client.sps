#!/usr/bin/env scheme-script
;; SPDX-License-Identifier: MIT

;; Connect to a port and send Hello

(import (srfi :106 socket)
        (rnrs))

(call-with-socket (make-client-socket (cadr (command-line))
                                      (caddr (command-line)))
   (lambda (p)
     (write p)
     (newline)
     (socket-send p (string->utf8 "Hello\n"))
     (write (utf8->string (socket-recv p 100)))
     (newline)))

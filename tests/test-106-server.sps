#!/usr/bin/env scheme-script
;; SPDX-License-Identifier: MIT

;; Connect to a port and send Hello

(import (srfi :106 socket)
        (rnrs))

(call-with-socket (make-server-socket (cadr (command-line)))
  (lambda (s)
    (write s)
    (newline)
    (call-with-socket (socket-accept s)
       (lambda (c)
         (write c)
         (newline)
         (socket-send c (socket-recv c 100))))))

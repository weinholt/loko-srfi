# Extra SRFIs for Loko Scheme

This repository provides implementations for SRFIs for Loko Scheme, an
optimizing R6RS compiler. A SRFI is a standardization process similar
to Python's PEP process.

## Installation

Install it with the [Akku package manager](https://akkuscm.org):

```
akku update
cd my-project
akku install loko-srfi
```

## SRFI 106 (Basic socket interface)

An implementation of SRFI 106 is provided, which can be used to create
clients and servers using sockets. It supports IPv4, IPv6, TCP, UDP
and asynchronous name resolution with DNS. Concurrent clients and
servers can be written using fibers.

Due to Loko Scheme not using the C runtime and/or standard library,
this implementation comes with its own DNS stub resolver. If your use
case requires integration with the name service switch, then please
open an issue.

### System integration

This library reads the following files:

* /etc/hosts
* /etc/resolv.conf
* /etc/services

They have the typical semantics found on GNU systems.

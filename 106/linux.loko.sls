;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; SRFI-106 (Socket) interface layer

;; This code is based on the reference implementation at
;; srfi.schemers.org.

;; Limitations:

;; No support for ai-flags
;; No support for the GNU libc name service switch

;; Support for nsswitch would likely require forking off a helper.
;; Maybe if dbus is not overcomplicated then it could also support the
;; stub resolver in systemd-networkd, which should hopefully handle
;; the nsswitch stuff.

;; TODO: Happy Eyeballs v2 (https://www.rfc-editor.org/info/rfc8305)
;; TODO: Proxy support

(library (srfi :106 compat)
  (export
    make-client-socket
    make-server-socket
    socket?
    call-with-socket

    *ai-passive*
    *ai-canonname*
    *ai-numerichost*
    *ai-v4mapped*
    *ai-all*
    *ai-addrconfig*

    (rename (fxior socket-merge-flags))
    socket-purge-flags

    socket-accept
    socket-send socket-send*
    socket-recv socket-recv!
    socket-shutdown
    socket-close
    socket-input-port
    socket-output-port
    (rename (AF_UNSPEC *af-unspec*)
            (AF_INET   *af-inet*)
            (AF_INET6  *af-inet6*)
            ;; Not part of SRFI 106
            (AF_LOCAL  *af-local*))
    (rename (SOCK_STREAM *sock-stream*)
            (SOCK_DGRAM  *sock-dgram*))
    (rename (IPPROTO_IP  *ipproto-ip*)
            (IPPROTO_TCP *ipproto-tcp*)
            (IPPROTO_UDP *ipproto-udp*))
    (rename (MSG_PEEK    *msg-peek*)
            (MSG_OOB     *msg-oob*)
            (MSG_WAITALL *msg-waitall*))
    (rename (SHUT_RD   *shut-rd*)
            (SHUT_WR   *shut-wr*)
            (SHUT_RDWR *shut-rdwr*)))
  (import
    (rnrs (6))
    (industria dns)
    (industria dns numbers)
    (industria dns types)
    (ip-address)
    (struct pack)
    (loko system fibers)
    (only (loko system unsafe) bytevector-address)
    (loko arch amd64 linux-numbers)
    (loko arch amd64 linux-syscalls))

(define NULL 0)

(define DNS-query-timeout 5)

;; These are from libc, which we don't have access to. We'll need to
;; figure things out ourselves.
(define *ai-passive*     #x0001)
(define *ai-canonname*   #x0002)
(define *ai-numerichost* #x0004)
(define *ai-v4mapped*    #x0008)
(define *ai-all*         #x0010)
(define *ai-addrconfig*  #x0020)

(define-record-type socket
  (sealed #t)
  (fields (mutable fd) address))

(define-syntax define-optional
  (lambda (x)
    (define (opt-clauses name args* opt*)
      (syntax-case opt* ()
        [() '()]
        [((lhs rhs) (lhs* rhs*) ...)
         (with-syntax ([(args ...) args*])
           #`([(args ...) (#,name args ... rhs)]
              #,@(opt-clauses name #'(args ... lhs) #'((lhs* rhs*) ...))))]))
    (syntax-case x ()
      [(_ (name args ... [(lhs* rhs*) ...])
          . body)
       #`(define name
           (case-lambda
             #,@(opt-clauses #'name #'(args ...) #'((lhs* rhs*) ...))
             [(args ... lhs* ...) . body]))])))

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

(define (socket-purge-flags x . y)
  (apply fxand x (map fxnot y)))

;;; Services, symbolic names for port numbers

;; Parse a line from a typical conf-space file into parts
(define (get-split-line p)
  (let next-line ()
    (let ((line (get-line p)))
      (if (eof-object? line)
          line
          (let ((p (open-string-input-port line)))
            (let lp ((ret '()) (c* '()))
              (let ((c (get-char p)))
                (cond ((or (eof-object? c) (eqv? c #\#) (char-whitespace? c))
                       (let ((ret (if (null? c*)
                                      ret
                                      (cons (list->string (reverse c*)) ret))))
                         (if (or (eof-object? c) (eqv? c #\#))
                             (if (null? ret)
                                 (next-line)
                                 (reverse ret))
                             (lp ret '()))))
                      (else
                       (lp ret (cons c c*)))))))))))

(define-record-type servent
  (sealed #t)
  (fields name port proto aliases))

;; Parse a line in /etc/services
(define (get-service-entry p)
  (let next-line ()
    (let ((parts (get-split-line p)))
      (if (eof-object? parts)
          parts
          (if (not (and (pair? parts) (pair? (cdr parts))))
              (next-line)
              (let* ((port/protocol (string-split (cadr parts) #\/))
                     (port (string->number (car port/protocol)))
                     (protocol (and (pair? (cdr port/protocol))
                                    (string->symbol (cadr port/protocol)))))
                (if (not (and port protocol))
                    (next-line p)
                    (make-servent (car parts) port protocol (cddr parts)))))))))

;; TODO: GNU/Linux systems use the name service switch, but that
;; requires libc. We can fork off a helper program if that becomes
;; important.
(define service->port
  (let ((cache (make-hashtable equal-hash equal?))) ;("ssh" . tcp) => 22
    (lambda (who service proto)
      (define (fail)
        (error who "Unknown service" service))
      (cond ((string->number service 10) =>
             (lambda (port)
               (if (fx<=? 0 port 65535)
                   port
                   (fail))))
            ((hashtable-ref cache (cons service proto) #f)
             => (lambda (port)
                  (if (eq? port 'no-entry)
                      (fail)
                      port)))
            ((not (file-exists? "/etc/services"))
             (fail))
            (else
             (call-with-input-file "/etc/services"
               (lambda (p)
                 (let lp ()
                   (let ((entry (get-service-entry p)))
                     (cond ((eof-object? entry)
                            (hashtable-set! cache (cons service proto) 'no-entry)
                            (fail))
                           ((and (or (not proto)
                                     (eq? (servent-proto entry) proto))
                                 (or (string=? (servent-name entry) service)
                                     (member service (servent-aliases entry))))
                            (hashtable-set! cache (cons service proto) (servent-port entry))
                            (servent-port entry))
                           (else (lp))))))))))))

;;; Local host database in /etc

(define-record-type hostent
  (sealed #t)
  (fields name aliases attrtype addr))

;; Parse a line in /etc/hosts
(define (get-hosts-entry p)
  (let next-line ()
    (let ((parts (get-split-line p)))
      (if (eof-object? parts)
          parts
          (if (not (and (pair? parts) (pair? (cdr parts))))
              (next-line)
              (let ((address (or (string->ipv4 (car parts))
                                 (string->ipv6 (car parts))))
                    (name (cadr parts))
                    (aliases (cddr parts)))
                (let ((addrtype (and address
                                     (case (bytevector-length address)
                                       ((4) AF_INET)
                                       (else AF_INET6)))))
                  (if (not (and address name addrtype))
                      (next-line)
                      (make-hostent name aliases addrtype address)))))))))

(define (local-host-lookup node)
  (and
    (file-exists? "/etc/hosts")
    (call-with-input-file "/etc/hosts"
      (lambda (p)
        ;; FIXME: should be cached
        (let lp ()
          (let ((ent (get-hosts-entry p)))
            (cond
              ((eof-object? ent) #f)
              ((or (string-ci=? node (hostent-name ent))
                   (exists (lambda (alias)
                             (string-ci=? node alias))
                           (hostent-aliases ent)))
               ent)
              (else (lp)))))))))

;;; DNS

;; Parse a line in /etc/resolv.conf
(define (get-resolv-setting p)
  (let next-line ()
    (let ((parts (get-split-line p)))
      (if (eof-object? parts)
          parts
          (if (not (pair? (cdr parts)))
              (next-line)
              (cons (string->symbol (car parts)) (cdr parts)))))))

;; DNS cache. This maps from pairs of queried hostname + rrtype to a
;; list of the resources in the answer section of the recursive
;; resolver's response. It does not mingle resources from queries for
;; different hostnames. There are potential security issues with
;; mingling, but it also complicates things.
(define (make-dns-cache)
  (make-hashtable (lambda (x) (+ (string-hash (car x)) (cdr x)))
                  equal?))

;; Record resp as the response to the query. It is either a
;; dns-message or the symbol fail, which indicates that all resolvers
;; failed to answer.
(define (dns-cache-update! cache hostname qtype resp)
  (let ((time 0))                       ;TODO: do something useful
    (hashtable-set! cache (cons hostname qtype)
                    (cons time
                          (if (dns-message? resp)
                              (filter (lambda (x)
                                        (eqv? (dns-resource-class x) (dns-class IN)))
                                      (dns-message-answer resp))
                              resp)))))

(define (dns-cache-lookup cache hostname rrtype)
  (cond
    ((hashtable-ref cache (cons hostname rrtype) #f) =>
     (lambda (time+answer)
       (let ((time (car time+answer)) (answer (cdr time+answer)))
         (let lp ((name (dns-labels->string (string->dns-labels hostname))))
           ;; Resp is the list of resources from the answer section of
           ;; the response from the recursive resolver. We still need to
           ;; recursively handle CNAMEs, but all the resources needed
           ;; for it are in the list. Returns a list of bytevectors,
           ;; which are either 4 or 16 long (A/AAAA).
           (if (pair? answer)
               (let ((rrs (filter (lambda (x)
                                    (let ((type (dns-resource-type x)))
                                      (and (or (eqv? type rrtype)
                                               (eqv? type (dns-rrtype CNAME)))
                                           (string-ci=? (dns-labels->string (dns-resource-name x))
                                                        name))))
                                  answer)))
                 (cond
                   ((and (pair? rrs)
                         (eqv? (dns-resource-type (car rrs)) (dns-rrtype CNAME)))
                    (lp (dns-labels->string (dns-resource/CNAME-name (car rrs)))))
                   (else
                    (map (lambda (rr)
                           (if (dns-resource/A? rr)
                               (dns-resource/A-address rr)
                               (dns-resource/AAAA-address rr)))
                         rrs))))
               '(fail))))))                ;XXX: all queries failed
    (else #f)))

;; Send a single DNS query and wait for a response or a timeout
(define (send-dns-query nameserver protocol qname qtype)
  (define (neterror? exn)
    (and (who-condition? exn)
         (memq (condition-who exn)
               '(make-client-socket socket-recv socket-send*))))
  (guard (exn
          ((neterror? exn)
           (write (list exn)) (newline)
           #f)
          (else
           (write exn)
           (newline)
           #f))
    (call-with-socket (make-client-socket nameserver "53" AF_INET
                                          (if (eq? protocol 'tcp)
                                              SOCK_STREAM SOCK_DGRAM))
      (lambda (s)
        (let ((q (make-normal-dns-query qname qtype #t)))
          (spawn-fiber
           (lambda ()
             (guard (exn ((neterror? exn) #f))
               (let ((o (socket-output-port s)))
                 (case protocol
                   ((tcp) (put-dns-message/delimited o q))
                   (else (put-dns-message o q)))
                 (flush-output-port o)))))
          (let ignore ()
            (let ((ch (make-channel)))
              (spawn-fiber
               (lambda ()
                 (put-message ch
                              (guard (exn ((neterror? exn) 'error))
                                (case protocol
                                  ((tcp) (get-bytevector-dns (socket-input-port s)))
                                  (else (socket-recv s 65535)))))))
              (let ((msg (perform-operation (choice-operation
                                             (wrap-operation (get-operation ch)
                                                             (lambda (x) (cons 'resp x)))
                                             (wrap-operation (sleep-operation DNS-query-timeout)
                                                             (lambda _ '(timeout)))))))
                (case (car msg)
                  ((timeout) 'timeout)
                  ((resp)
                   (if (eq? (cdr msg) 'error)
                       #f
                       (let ((r (parse-dns-message (cdr msg))))
                         (if (falsified-dns-reply? q r)
                             (ignore)
                             r)))))))))))))

;; Loop over all resolvers and ask each one (until one succeeds) for
;; the hostname's A or AAAA record. Responses and failures are send to
;; query-resp-ch.
(define (dns-do-query hostname qtype resolvers query-resp-ch received-AAAA-cvar)
  (define (message-ok? resp)
    (and (dns-message? resp)
         (eqv? 0 (fxand (dns-message-flags resp) flag-TC))))
  (let lp ((resolvers resolvers))
    ;; If flag-TC is set then we should retry with TCP
    (cond
      ((null? resolvers)
       (put-message query-resp-ch (list 'fail hostname qtype)))
      (else
       (let* ((resolver (car resolvers))
              (resp (send-dns-query resolver 'udp hostname qtype))
              (resp (cond ((not (message-ok? resp))
                           (send-dns-query resolver 'tcp hostname qtype))
                          (else resp))))
         (cond
           ((message-ok? resp)
            (cond
              ((eqv? qtype (dns-rrtype AAAA))
               (put-message query-resp-ch (list 'ok hostname qtype resp))
               (signal-cvar! received-AAAA-cvar))
              (else
               ;; Prefer AAAA by slightly delaying the response for
               ;; the A query. This is a "Happy Eyeballs v2" thing.
               (perform-operation (choice-operation
                                   (wait-operation received-AAAA-cvar)
                                   (sleep-operation #e50/1000)))
               (put-message query-resp-ch (list 'ok hostname qtype resp)))))
           (else
            (lp (cdr resolvers)))))))))

(define (make-dns-resolver req-ch)
  (lambda ()
    (define config
      (call-with-input-file "/etc/resolv.conf"
        (lambda (p)
          (let lp ()
            (let ((line (get-resolv-setting p)))
              (if (eof-object? line)
                  '()
                  (cons line (lp))))))))
    (define resolvers                   ;all trusted resolvers
      (let ((ip* (map cadr (filter
                            (lambda (x)
                              (and (eq? (car x) 'nameserver)
                                   (or (string->ipv4 (cadr x))
                                       ;; XXX: drops addresses with
                                       ;; a scope, but should it?
                                       (string->ipv6 (cadr x)))))
                            config))))
        (if (null? ip*) '("127.0.0.1") ip*)))
    (define cache (make-dns-cache))
    (define query-resp-ch (make-channel))  ;all DNS responses arrive here
    (define (process-pending pending)
      (filter
       (lambda (req)
         (let ((resp-ch (car req)) (node (cdr req)))
           (let ((cached-A (dns-cache-lookup cache node (dns-rrtype A)))
                 (cached-AAAA (dns-cache-lookup cache node (dns-rrtype AAAA))))
             (cond
               ((or (not cached-A) (not cached-AAAA))
                ;; A response is still pending
                #t)
               (else
                ;; FIXME: Sort the addresses by the rules
                (let ((ip* (filter bytevector? (append cached-A cached-AAAA))))
                  (if (pair? ip*)
                      (put-message resp-ch (cons 'ok ip*))
                      ;; TODO: Indicate what the error actually was
                      ;; (i.e dysfunctional recursive resolvers or
                      ;; errors from the servers)
                      (put-message resp-ch
                                   (cons 'fail
                                         (if (and (null? cached-A) (null? cached-AAAA))
                                             "name not found"
                                             "all resolvers failed")))))
                #f)))))
       pending))
    ;; TODO: Either expire cache regularly or refresh the cache
    ;; regularly (for entries that are still being queried)
    (let lp ((pending '()))
      (let ((msg (perform-operation
                  (choice-operation (wrap-operation (get-operation req-ch)
                                                    (lambda (x) (cons 'req x)))
                                    (wrap-operation (get-operation query-resp-ch)
                                                    (lambda (x) (cons 'resp x)))))))
        (case (car msg)
          ((req)
           ;; Received a request to resolve a hostname.
           (let ((req (cdr msg)))
             (cond
               ((null? (process-pending (list req)))
                ;; It was answered from the cache
                (lp pending))
               (else
                (let ((resp-ch (car req)) (node (cdr req))
                      (received-AAAA-cvar (make-cvar)))
                  ;; Send AAAA and A queries, trying each resolver in turn.
                  ;; When a response arrives it is sent to query-resp-ch.
                  ;; Same with failures. TODO: Skip AAAA if the system does
                  ;; not have IPv6
                  (for-each (lambda (qtype)
                              (spawn-fiber
                               (lambda ()
                                 (dns-do-query node qtype resolvers
                                               query-resp-ch received-AAAA-cvar))))
                            (list (dns-rrtype AAAA) (dns-rrtype A)))
                  (lp (cons req pending)))))))
          ((resp)
           ;; Received a response from a resolver. Put it into the
           ;; cache and process pending queries.
           (let* ((resp (cdr msg))
                  (status (car resp)) (hostname (cadr resp)) (qtype (caddr resp)))
             (if (eq? status 'ok)
                 (let ((dns-resp (cadddr resp)))
                   ;; (print-dns-message dns-resp)
                   (dns-cache-update! cache hostname qtype dns-resp))
                 (dns-cache-update! cache hostname qtype 'fail))
             (lp (process-pending pending))))
          (else (lp pending)))))))

(define dns-host-lookup
  (let ((inited? #f)
        (req-ch (make-channel)))
    (lambda (who node service)
      (unless inited?
        (set! inited? #t)
        (spawn-fiber (make-dns-resolver req-ch)))
      (let ((resp-ch (make-channel)))
        (put-message req-ch (cons resp-ch node))
        (let ((resp (get-message resp-ch)))
          (case (car resp)
            ((ok) (cdr resp))
            (else
             (raise
               (condition
                (make-who-condition who)
                (make-message-condition
                 (string-append "DNS resolution error: " (cdr resp)))
                (make-irritants-condition (list node service)))))))))))

;;; Sockets

(define (sockaddr-family buf)
  (unpack "S" buf))

(define (sockaddr->string buf)
  (define (sockaddr_in6->string buf)
    (let ((ip (make-bytevector 16)))
      (bytevector-copy! buf offsetof-sockaddr_in6-sin6_addr ip 0 16)
      (let ((port (unpack "!S" buf offsetof-sockaddr_in6-sin6_port)))
        (string-append "[" (ipv6->string ip) "]:" (number->string port)))))
  (define (sockaddr_in->string buf)
    (let ((ip (make-bytevector 4)))
      (pack! "L" ip 0 (unpack "L" buf offsetof-sockaddr_in-sin_addr))
      (let ((port (unpack "!S" buf offsetof-sockaddr_in-sin_port)))
        (string-append (ipv4->string ip) ":" (number->string port)))))
  (let ((family (sockaddr-family buf)))
    (cond ((eqv? family AF_INET)
           (sockaddr_in->string buf))
          ((eqv? family AF_INET6)
           (sockaddr_in6->string buf))
          ((eqv? family AF_LOCAL)
           (let ((s (utf8->string buf)))
             (substring s 2 (string-length s))))
          (else
           (call-with-string-output-port
             (lambda (p)
               (display "unknown family " p)
               (display buf p)))))))

;; Create a sockaddr for a Unix domain socket. On Linux, if the first
;; character is a NUL character then it refers to an abstract address.
(define (make-sockaddr/un who fn)
  (unless (or (string? fn) (not (equal? fn "")))
    (assertion-violation who "Expected a non-empty string" fn))
  (string-for-each
   (lambda (c)
     (when (eqv? c #\nul)
       (raise (condition
               (make-who-condition who)
               (make-i/o-filename-error fn)
               (make-message-condition "Unrepresentable filename")
               (make-irritants-condition (list fn))))))
   (substring fn 1 (string-length fn)))
  (let ((filename-bv (string->utf8 fn)))
    (unless (fx<? (bytevector-length filename-bv)
                  (fx- sizeof-sockaddr_un offsetof-sockaddr_un-sun_path))
      (assertion-violation who "The filename is too long" fn))
    (let ((addr (make-bytevector (fx+ 2 (bytevector-length filename-bv)) 0)))
      (bytevector-u16-native-set! addr offsetof-sockaddr_un-sun_family AF_LOCAL)
      ;; XXX: Linux can handle a missing NUL character at the very end
      ;; of the buffer.
      (bytevector-copy! filename-bv 0
                        addr offsetof-sockaddr_un-sun_path
                        (bytevector-length filename-bv))
      addr)))

(define (make-sockaddr/inet ip port)
  (let ((addr (make-bytevector sizeof-sockaddr_in)))
    (bytevector-u16-native-set! addr offsetof-sockaddr_in-sin_family AF_INET)
    (bytevector-u16-set! addr offsetof-sockaddr_in-sin_port port (endianness big))
    (bytevector-copy! ip 0 addr offsetof-sockaddr_in-sin_addr (bytevector-length ip))
    addr))

(define (make-sockaddr/inet6 ip port scope-id)
  (let ((addr (make-bytevector sizeof-sockaddr_in6)))
    (bytevector-u16-native-set! addr offsetof-sockaddr_in6-sin6_family AF_INET6)
    (bytevector-u16-set! addr offsetof-sockaddr_in6-sin6_port port (endianness big))
    (bytevector-copy! ip 0 addr offsetof-sockaddr_in6-sin6_addr (bytevector-length ip))
    (bytevector-u32-native-set! addr offsetof-sockaddr_in6-sin6_scope_id scope-id)
    addr))

(define (get-sockaddr-list who node service ai-family proto)
  (cond
    ((eqv? ai-family AF_LOCAL)
     (list (make-sockaddr/un who node)))
    ((or (eqv? ai-family AF_INET) (eqv? ai-family AF_INET6))
     (let ((port (service->port who service proto))
           (node/scope (string-split node #\%)))
       (let ((node (car node/scope))
             (scope (if (pair? (cdr node/scope)) (cadr node/scope) #f))
             (ip* (cond ((string->ipv4 node) => list)
                        ((string->ipv6 node) => list)
                        ((local-host-lookup node) =>
                         (lambda (x) (list (hostent-addr x))))
                        ;; Cheating, cheating, cheating.
                        ((string-ci=? node "localhost") (list #vu8(127 0 0 1)))
                        ((dns-host-lookup who node service))
                        (else
                         (error who "Name resolution failed" node service)))))
         (map (lambda (ip)
                (cond ((eqv? (bytevector-length ip) 16)
                       ;; TODO: Look up the scope id
                       (make-sockaddr/inet6 ip port 0))
                      ((eqv? (bytevector-length ip) 4)
                       (make-sockaddr/inet ip port))
                      (else
                       (error who "Unknown address returned" ip))))
              ip*))))
    (else
     (assertion-violation who "Unknown address family" node service))))

(define (accept fd addr addrlen flags)
  (sys_accept4 fd (bytevector-address addr) (bytevector-address addrlen) flags
               (lambda (errno)
                 (cond ((eqv? errno EAGAIN)
                        (wait-for-readable fd)
                        (accept fd addr addrlen flags))
                       ((eqv? errno EINTR)
                        (accept fd addr addrlen flags))
                       (else
                        (raise (condition
                                (make-syscall-error 'accept4 errno)
                                (make-irritants-condition
                                 (list fd addr addrlen flags)))))))))

(define (setsockopt fd level optname optval)
  (sys_setsockopt fd level optname (bytevector-address optval) (bytevector-length optval)))

(define (ai-junk->protocol ai-family ai-socktype ai-protocol)
  (cond ((eqv? ai-protocol IPPROTO_UDP) 'udp)
        ((eqv? ai-protocol IPPROTO_TCP) 'tcp)
        ((eqv? ai-protocol IPPROTO_SCTP) 'sctp)
        (else #f)))

(define-optional (make-client-socket node service
                                     [(ai-family AF_INET)
                                      (ai-socktype SOCK_STREAM)
                                      (ai-flags (fxior *ai-v4mapped* *ai-addrconfig*))
                                      (ai-protocol IPPROTO_IP)])
  ;; XXX: The ai- flags are for address information lookup
  (define (try-connect addr*)
    ;; Try to connect to each address in turn. There is at least one
    ;; address. Be permissive about all errors, moving on to the next
    ;; address, except for when we're on the last address. TODO:
    ;; Record the errors and make better condition records for them
    (let* ((addr (car addr*))
           ;; FIXME: be permissive about failure to make a socket
           (fd (sys_socket (sockaddr-family addr)
                           (fxior ai-socktype SOCK_CLOEXEC SOCK_NONBLOCK)
                           ai-protocol))
           (status
            (let retry ()
              (sys_connect fd (bytevector-address addr) (bytevector-length addr)
                           (lambda (errno)
                             (cond ((eqv? errno EINTR)
                                    (retry))
                                   ((eqv? errno EINPROGRESS)
                                    (wait-for-writable fd)
                                    (let* ((statusbv (make-bytevector sizeof-int))
                                           (statuslen (if (eqv? sizeof-void* 8)
                                                          (pack "Q" sizeof-int)
                                                          (pack "L" sizeof-int))))
                                      (sys_getsockopt fd SOL_SOCKET SO_ERROR
                                                      (bytevector-address statusbv)
                                                      (bytevector-address statuslen))
                                      (let ((status (if (eqv? sizeof-int 8)
                                                        (unpack "Q" statusbv)
                                                        (unpack "L" statusbv))))
                                        (cond
                                          ((eqv? status 0)
                                           0)
                                          (else
                                           (sys_close fd)
                                           (if (null? (cdr addr*))
                                               (raise (condition
                                                       (make-who-condition 'make-client-socket)
                                                       (make-syscall-error 'connect status)
                                                       (make-irritants-condition
                                                        (list node service (sockaddr->string addr)))))
                                               'try-next))))))
                                   ((eqv? errno EAGAIN)
                                    ;; This is for local sockets.
                                    ;; FIXME: Manpages are unclear.
                                    (wait-for-writable fd)
                                    (retry))
                                   (else
                                    (sys_close fd)
                                    (if (null? (cdr addr*))
                                        (raise (condition
                                                (make-who-condition 'make-client-socket)
                                                (make-syscall-error 'connect errno)
                                                (make-irritants-condition
                                                 (list node service (sockaddr->string addr)))))
                                        'try-next))))))))
      (if (eq? status 'try-next)
          (try-connect (cdr addr*))
          (values fd addr))))
  (assert (string? node))
  (assert (string? service))
  (let* ((addr* (get-sockaddr-list 'make-client-socket node service ai-family
                                   (ai-junk->protocol ai-family ai-socktype ai-protocol))))
    (let-values ([(fd addr) (try-connect addr*)])
      (make-socket fd (sockaddr->string addr)))))

(define-optional (make-server-socket service
                                     [(ai-family AF_INET)
                                      (ai-socktype SOCK_STREAM)
                                      (ai-protocol IPPROTO_IP)])
  (let ((addr (car (get-sockaddr-list 'make-server-socket
                                      (if (eqv? ai-family AF_INET) "0.0.0.0" "::")
                                      service ai-family
                                      (ai-junk->protocol ai-family ai-socktype ai-protocol)))))
    (let ((fd (sys_socket ai-family
                          (fxior ai-socktype SOCK_CLOEXEC SOCK_NONBLOCK)
                          ai-protocol)))
      (let ((optval (pack "l" 1)))
        (sys_setsockopt fd SOL_SOCKET SO_REUSEADDR
                        (bytevector-address optval) (bytevector-length optval)))
      (sys_bind fd (bytevector-address addr) (bytevector-length addr))
      (sys_listen fd 1000)
      (make-socket fd (sockaddr->string addr)))))

(define (call-with-socket socket proc)
  (assert (socket? socket))
  (let-values ([v (proc socket)])
    (socket-close socket)
    (apply values v)))

(define (assert-open who socket)
  (unless (socket-fd socket)
    (assertion-violation who "The socket is closed" socket)))

(define (socket-accept socket)
  (assert-open 'socket-accept socket)
  (let ((addr (make-bytevector sizeof-sockaddr_in6))
        (addrlen (pack "q" sizeof-sockaddr_in6))
        (flags (bitwise-ior SOCK_NONBLOCK SOCK_CLOEXEC))
        (sfd (socket-fd socket)))
    (let ((fd (let retry ()
                (sys_accept4 sfd (bytevector-address addr)
                             (bytevector-address addrlen) flags
                             (lambda (errno)
                               (cond ((eqv? errno EAGAIN)
                                      (wait-for-readable sfd)
                                      (retry))
                                     ((eqv? errno EINTR)
                                      (retry))
                                     (else
                                      (raise (condition
                                              (make-who-condition 'socket-accept)
                                              (make-syscall-error 'accept4 errno)
                                              (make-irritants-condition (list socket)))))))))))
      (make-socket fd (sockaddr->string addr)))))

(define-optional (socket-send socket buf [(flags 0)])
  (assert-open 'socket-send socket)
  (sys_sendto (socket-fd socket) (bytevector-address buf) (bytevector-length buf)
              flags NULL 0
              (lambda (errno)
                (cond ((eqv? errno EAGAIN)
                       (wait-for-writable (socket-fd socket))
                       (socket-send socket buf flags))
                      ((eqv? errno EINTR)
                       (socket-send socket buf flags))
                      (else
                       (raise (condition
                               (make-who-condition 'socket-send)
                               (make-syscall-error 'sendto errno)
                               (make-irritants-condition
                                (list (socket-fd socket))))))))))

(define (socket-send* socket buf start count) ;not standard
  (define flags 0)
  (assert (fx<=? 0 start (fx+ start count) (bytevector-length buf)))
  (assert-open 'socket-send* socket)
  (sys_sendto (socket-fd socket) (fx+ (bytevector-address buf) start) count
              flags NULL 0
              (lambda (errno)
                (cond ((eqv? errno EAGAIN)
                       (wait-for-writable (socket-fd socket))
                       (socket-send socket buf flags))
                      ((eqv? errno EINTR)
                       (socket-send socket buf flags))
                      (else
                       (raise (condition
                               (make-who-condition 'socket-send)
                               (make-syscall-error 'sendto errno)
                               (make-irritants-condition
                                (list (socket-fd socket))))))))))

(define-optional (socket-recv socket size [(flags 0)])
  (assert-open 'socket-recv socket)
  (let ((buf (make-bytevector size)))
    (let ((len (let retry ()
                 (sys_recvfrom (socket-fd socket) (bytevector-address buf)
                               (bytevector-length buf)
                               flags NULL 0
                               (lambda (errno)
                                 (cond ((eqv? errno EAGAIN)
                                        (wait-for-readable (socket-fd socket))
                                        (retry))
                                       ((eqv? errno EINTR)
                                        (retry))
                                       (else
                                        (raise (condition
                                                (make-who-condition 'socket-recv)
                                                (make-syscall-error 'recvfrom errno)
                                                (make-irritants-condition
                                                 (list (socket-fd socket))))))))))))
      ;; TODO: Truncate buf in-place instead
      (if (fx<? len size)
          (let ((ret (make-bytevector len)))
            (bytevector-copy! buf 0 ret 0 len)
            ret)
          buf))))

(define (socket-recv! socket buf start size) ;not standard
  (define flags 0)
  (assert (fx<=? 0 start (fx+ start size) (bytevector-length buf)))
  (assert-open 'socket-recv! socket)
  (let lp ()
    (sys_recvfrom (socket-fd socket) (fx+ start (bytevector-address buf))
                  size flags NULL 0
                  (lambda (errno)
                    (cond ((eqv? errno EAGAIN)
                           (wait-for-readable (socket-fd socket))
                           (lp))
                          ((eqv? errno EINTR)
                           (lp))
                          (else
                           (raise (condition
                                   (make-who-condition 'socket-recv)
                                   (make-syscall-error 'recvfrom errno)
                                   (make-irritants-condition
                                    (list (socket-fd socket)))))))))))

(define (socket-shutdown socket how)
  (assert-open 'socket-shutdown socket)
  (sys_shutdown (socket-fd socket) how))

(define (socket-close socket)
  (cond ((socket-fd socket) =>
         (lambda (fd)
           (socket-fd-set! socket #f)
           (sys_close fd)))))

(define (socket-input-port socket)
  (define (read! bv start count)
    (socket-recv! socket bv start count))
  (make-custom-binary-input-port "socket-input-port" read! #f #f #f))

(define (socket-output-port socket)
  (define (write! bv start count)
    (socket-send* socket bv start count))
  (make-custom-binary-output-port "socket-output-port" write! #f #f #f)))
